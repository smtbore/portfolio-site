import React from 'react';

const HomePage = () => (
  <>
    <h1>Steven Bore - Portfolio Site</h1>
    <p>
      Welcome to my portfolio site! This is my first attempt at a proper
      react build. Eventually, this website will display my past and current development
      projects in my journey as a web developer!
    </p>
    <p>
      The key skills i want to learn from this first build is: 
      <ol>
        <li>Create a functioning navigation bar using react-router links</li>
        <li>Understand and use react components within different heirarchys of the project</li>
        <li>Use the JSX language react uses to produce basic content on each page on the site</li>
        <li>If time permits, I would like to attempt to link a back-end to this front-end design so future portfolio work
          can be uploaded!
        </li>
      </ol>
    </p>
  </>
  // wraps content in a fragment < 1 element is being returned
);

export default HomePage;
