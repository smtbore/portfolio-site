import React from 'react';
import Photo from './Assets/Photo1.jpg';
import "./style.css";

const AboutPage = () => ( //stores all jsx code for the page in a variable
  <>
    <h1>About Me</h1>
    <img src={Photo}></img> {/*imports an image using the imported Photo module*/}
    <h2>Introduction</h2>
    <p>
      Hello, my name is Steven Bore and I am a 22 Year Old front-end developer!
      I have recently finished studying an undergraduate degree at edge hill
      univeristy. I am currently located in the north-west of england in
      Manchester! {/*Content */}
    </p>
    <h2 style={{marginTop: "2.2em"}}>Personal Interests</h2>  {/*JSX in-line styling used to put next section below image, notably different to html*/}
    <p>
      I am a very active individual, playing multiple sports (namely badminton and cricket) at club level! 
      I also spend a lot of time reading non-fiction, usually science and technology, journals. A personal
      favourite of mine is <a href="https://ourworldindata.org/#entries">Our World in Data</a>!
    </p>
</>
  // wraps content in a fragment because >1 element is being returned as a replacement for a div
);

export default AboutPage; //exported AboutPage module